<?php

class PillController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','view','create','update','delete','getPills','setDefault'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the drug id to associate this pill to
	 */
	public function actionCreate($id)
	{
		$model=new Pill;
		$model->drug_id=$id;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Pill'])) {
			$model->attributes=$_POST['Pill'];
			//$model->drug_id=$id;
			if ($model->save()) {
				$this->redirect(array('drug/view','id'=>$id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Pill'])) {
			$model->attributes=$_POST['Pill'];
			if ($model->save()) {
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model=$this->loadModel($id);
		// we only allow deletion via POST request
		if (Yii::app()->request->isPostRequest)
		{
			if (Helpers::checkAuth($model)) 
			{
				$model->delete();

				// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
				if (!isset($_GET['ajax'])) {
					$this->redirect(array('index'));
				} else {
					$this->redirect(array('admin'));
				}
			} else {
				throw new CHttpException(400,'Invalid request.');
			}
		} else {
			throw new CHttpException(400,'Invalid request.');
		}
	}


	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Pill');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Pill('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Pill'])) {
			$model->attributes=$_GET['Pill'];
		}

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Pill the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Pill::model()->findByPk($id);
		if ($model===null) {
			throw new CHttpException(404,'The requested page does not exist.');
		}
		return $model;
	}

	/**
	 * Called from dose/create by ajax to retrieve pills for a given drug_id
	 * @throws CHttpException
	 */
	public function actionGetPills()
	{
		if(!Yii::app()->request->isAjaxRequest)
			throw new CHttpException(400,'Invalid request');
		$drug_id=$_POST['Dose']['drug_id'];
		
		$criteria=new CDbCriteria();
		//$criteria->select='id,name';
		
		$criteria->condition='drug_id=:id';
		$criteria->params=array(':id'=>$drug_id);
		
		$pills=Pill::model()->findAll($criteria);
		foreach($pills as $pill)
		{
			echo TbHtml::tag('option', array('value'=>$pill->id,'selected'=>$pill->default), TbHtml::encode($pill->name), true);

		}
	}
	
	/**
	 * Called from drug/view to toggle default status for a given pill
	 * @throws CHttpException
	 */
	public function actionSetDefault()
	{
		if(!Yii::app()->request->isAjaxRequest)
			throw new CHttpException(400,'Invalid request');
		$pill_id=$_POST['id'][0];
		$drug_id=$_POST['drug_id'];
		
		$criteria=new CDbCriteria();
		$criteria->condition='drug_id=:drug_id';
		$criteria->params=array(':drug_id'=>$drug_id);
		
		$pills=Pill::model()->findAll($criteria);
		foreach($pills as $pill)
		{
			if($pill->id===$pill_id)
			{
				$pill->default=true;
				$pill->save(false);
			}
			else
			{
				$pill->default=false;
				$pill->save(false);
			}
		}
		
	}
	
	/**
	 * Performs the AJAX validation.
	 * @param Pill $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax']==='pill-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}