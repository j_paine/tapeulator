<?php

class DrugController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'index' 'view' 'create' 'update' 'delete' actions
				'actions'=>array('index','view','create','update','delete','getDoses'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' actions
				'actions'=>array('admin'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model=$this->loadModel($id);
		if(!Helpers::checkAuth($model))
			throw new CHttpException(400,'Invalid request.');
		
		$tapers=new CActiveDataProvider('Taper',array(
			'criteria'=>array(
				'with'=>'drug',
				'condition'=>'drug_id=:id',
				'params'=>array(':id'=>$id),
				'order'=>'active DESC, completed DESC',
			),
		));
		$pills=new CActiveDataProvider('Pill',array(
			'criteria'=>array(
				'condition'=>'drug_id=:id',
				'params'=>array(':id'=>$id),
				'order'=>'name',
			),
		));
		
		$this->render('view',array(
			'model'=>$model,
			'tapers'=>$tapers,
			'pills'=>$pills,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Drug;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Drug'])) {
			$model->attributes=$_POST['Drug'];
			$model->user_id=Yii::app()->user->id;
			if ($model->save()) {
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		if(!Helpers::checkAuth($model) || !Helpers::checkAdmin())
			throw new CHttpException(400,'Invalid request');
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Drug'])) {
			$model->attributes=$_POST['Drug'];
			if ($model->save()) {
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model=$this->loadModel($id);
		// we only allow deletion via POST request
		if (Yii::app()->request->isPostRequest)
		{
			if (Helpers::checkAuth($model)) 
			{
				$model->delete();

				// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
				if (!isset($_GET['ajax'])) {
					$this->redirect(array('index'));
				} else {
					$this->redirect(array('admin'));
				}
			} else {
				throw new CHttpException(400,'Invalid request');
			}
		} else {
			throw new CHttpException(400,'Invalid request');
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		// Limit results to those belonging to the user unless user is admin
		if(Helpers::checkAdmin())
		{
			$dataProvider=new CActiveDataProvider('Drug');
		} else {
			$dataProvider=new CActiveDataProvider(Drug::model()->userOwns());
		}
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		if(!Helpers::checkAdmin())
			throw new CHttpException(400,'Invalid request');
		$model=new Drug('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Drug'])) {
			$model->attributes=$_GET['Drug'];
		}

		$this->render('admin',array(
			'model'=>$model,
		));
	}
        
        /**
         * Call by ajax to get dose history for drug
         */
        public function actionGetDoses($id)
        {
            //$drug=$this->loadModel($id);
            $data=array();
            $criteria=new CDbCriteria();
            $criteria->with='pill';
            $criteria->together=true;
            $criteria->condition='pill.drug_id=:id';
            $criteria->params=array(':id'=>$id);
            $criteria->order='date DESC';
            $doses=Dose::model()->findAll($criteria);
            foreach($doses as $dose)
            {
                $data[]=array(
                    'id'=>$dose->id,
                    'dose'=>Pill::getDose($dose->pill_id, $dose->weight),
                    'date'=>date_parse($dose->date),
                );
            }
            $this->renderJSON($data);
        }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Drug the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Drug::model()->findByPk($id);
		if ($model===null) {
			throw new CHttpException(404,'The requested page does not exist.');
		}
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Drug $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax']==='drug-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}