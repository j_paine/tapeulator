<?php

/**
 * This is the model class for table "drug".
 *
 * The followings are the available columns in table 'drug':
 * @property integer $id
 * @property string $name
 * @property string $nick
 * @property double $half_life_low
 * @property double $half_life_high
 * @property integer $user_id
 * @property string $created
 * @property string $updated
 *
 * The followings are the available model relations:
 * @property User $user
 * @property Pill[] $pills
 * @property Taper[] $tapers
 */
class Drug extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'drug';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('half_life_low, half_life_high', 'numerical'),
			array('name, nick', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, nick, half_life_low, half_life_high, user_id, created, updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'pills' => array(self::HAS_MANY, 'Pill', 'drug_id'),
			'tapers' => array(self::HAS_MANY, 'Taper', 'drug_id'),
		);
	}
	
	public function behaviors()
	{
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'created',
				'updateAttribute' => 'updated',
			)
		);
	}
	
	public function scopes()
	{
		return array(
			'userOwns'=>array(
				'condition'=>'user_id=:id',
				'params'=>array(':id'=>Yii::app()->user->id),
			),
		);
	}
	
	/**
	 * Check user has permission to delete this record
	 * @return boolean
	 */
	public function beforeDelete()
	{
		if(!Helpers::checkAdmin() || !Helpers::checkAuth($this))
			return false;
		parent::beforeDelete();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'nick' => 'Common Name',
			'half_life_low' => 'Half-life Low',
			'half_life_high' => 'Half-life High',
			'user_id' => 'User',
			'created' => 'Created',
			'updated' => 'Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('nick',$this->nick,true);
		$criteria->compare('half_life_low',$this->half_life_low);
		$criteria->compare('half_life_high',$this->half_life_high);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public static function getDrugs()
	{
		$criteria=new CDbCriteria;
		$criteria->select='id,name';
		$criteria->order='name';
		
		if(!Helpers::checkAdmin())
		{
			return Drug::model()->userOwns()->findAll($criteria);
		} else {
			return Drug::model()->findAll($criteria);
		}
		
	}
	
	public static function getCurrentTaper($id)
	{
		$model=Drug::model()->findByPk($id);
		foreach($model->tapers as $taper)
		{
			if($taper->active)
				return $taper;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Drug the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
