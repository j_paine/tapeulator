<?php

/**
 * This is the model class for table "dose".
 *
 * The followings are the available columns in table 'dose':
 * @property integer $id
 * @property integer $pill_id
 * @property integer $taper_id
 * @property integer $user_id
 * @property double $weight
 * @property string $date
 * @property string $created
 * @property string $updated
 *
 * The followings are the available model relations:
 * @property Comment[] $comments
 * @property Taper $taper
 * @property Pill $pill
 * 
 * Attributes
 * @property integer $drug_id
 */
class Dose extends CActiveRecord
{
	
	public $drug_id;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dose';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pill_id, weight, date', 'required'),
			array('date', 'date', 'format'=>'yyyy-MM-dd'),
			array('weight', 'match', 'pattern'=>'/^[0-9.,\n]*$/',
				'message'=>'Dose must contain only numbers separated by newlines'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, pill_id, taper_id, user_id, weight, date, created, updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'comments' => array(self::HAS_MANY, 'Comment', 'dose_id'),
			'taper' => array(self::BELONGS_TO, 'Taper', 'taper_id'),
			'pill' => array(self::BELONGS_TO, 'Pill', 'pill_id'),
			'user'=>array(self::BELONGS_TO,'User','user_id'),
		);
	}
	
	public function behaviors()
	{
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'created',
				'updateAttribute' => 'updated',
			)
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'drug_id'=>'Drug',
			'pill_id' => 'Pill',
			'taper_id' => 'Taper',
			'user_id' =>'User',
			'weight' => 'Pill Weight (mg)',
			'date' => 'Date',
			'created' => 'Created',
			'updated' => 'Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('pill_id',$this->pill_id);
		$criteria->compare('taper_id',$this->taper_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('weight',$this->weight);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Dose the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
