<?php

/**
 * This is the model class for table "pill".
 *
 * The followings are the available columns in table 'pill':
 * @property integer $id
 * @property string $name
 * @property double $weight
 * @property double $empty_weight
 * @property double $dose
 * @property integer $drug_id
 * @property boolean $default
 * @property string $created
 * @property string $updated
 *
 * The followings are the available model relations:
 * @property Dose[] $doses
 * @property Drug $drug
 */
class Pill extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pill';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, weight, dose, drug_id', 'required'),
			array('weight, empty_weight, dose', 'numerical'),
			array('default', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, weight, empty_weight, dose, drug_id, default, created, updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'doses' => array(self::HAS_MANY, 'Dose', 'pill_id'),
			'drug' => array(self::BELONGS_TO, 'Drug', 'drug_id'),
		);
	}
	
	public function behaviors()
	{
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'created',
				'updateAttribute' => 'updated',
			)
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'weight' => 'Weight (mg)',
			'empty_weight' => 'Capsule Weight (mg)',
			'dose' => 'Dose (mg)',
			'drug_id' => 'Drug',
			'default'=>'Default',
			'created' => 'Created',
			'updated' => 'Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('weight',$this->weight);
		$criteria->compare('empty_weight',$this->empty_weight);
		$criteria->compare('dose',$this->dose);
		$criteria->compare('drug_id',$this->drug_id);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public static function getPills($drugId)
	{
		if(!$_POST['ajax'])
			throw new CHttpException(400,'Invalid request');
		$criteria=new CDbCriteria;
		$criteria->select='id,name';
		$criteria->with=array('drug');
		$criteria->together=true;
		$criteria->condition='drug.id=:id';
		$criteria->params=array(':id'=>$drugId);
		$pills=Pill::model()->findAll($criteria);
		
		return $pills;
	}
	
	/**
	* Calculates the dose contained in a given weight of pill
	*/
	public static function getDose($pillId,$weight)
	{
	   $pill=Pill::model()->findByPk($pillId);
	   $fillerWeight=$pill->weight - $pill->empty_weight;
	   $doseWeight=$weight - $pill->empty_weight;
	   $dose=($doseWeight / $fillerWeight) * $pill->dose;

	   return $dose;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pill the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
