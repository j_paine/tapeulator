<?php
/* @var $this PillController */
/* @var $model Pill */
?>

<?php
$this->breadcrumbs=array(
	array('label'=>'Drugs','url'=>array('/drug')),
	array('label'=>$model->drug->name, 'url'=>array('/drug/view','id'=>$model->drug->id)),
	array('label'=>$model->name,'url'=>array('view','id'=>$model->id)),
	array('label'=>'Update'),
);

$this->menu=array(
	array('label'=>'List Pill', 'url'=>array('index')),
	array('label'=>'Create Pill', 'url'=>array('create')),
	array('label'=>'View Pill', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Pill', 'url'=>array('admin')),
);
?>

    <h1>Update Pill <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>