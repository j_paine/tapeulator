<?php
/* @var $this PillController */
/* @var $model Pill */


$this->breadcrumbs=array(
	array('label'=>'Pills','url'=>array('index')),
	array('label'=>'Manage'),
);

$this->menu=array(
	array('label'=>'List Pill', 'url'=>array('index')),
	array('label'=>'Create Pill', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#pill-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Pills</h1>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
        &lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'pill-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		'weight',
		'empty_weight',
		'dose',
		'drug.name',
		'created',
		'updated',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>