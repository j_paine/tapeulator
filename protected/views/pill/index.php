<?php
/* @var $this PillController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	array('label'=>'Pills'),
);

$this->menu=array(
	array('label'=>'Create Pill','url'=>array('create')),
	array('label'=>'Manage Pill','url'=>array('admin')),
);
?>

<h1>Pills</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>