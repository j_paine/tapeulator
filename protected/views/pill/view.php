<?php
/* @var $this PillController */
/* @var $model Pill */
?>

<?php
$this->breadcrumbs=array(
	array('label'=>'Drugs','url'=>array('/drug')),
	array('label'=>$model->drug->name, 'url'=>array('/drug/view','id'=>$model->drug->id)),
	array('label'=>$model->name),
);

$this->menu=array(
	array('label'=>'List Pill', 'url'=>array('index')),
	array('label'=>'Create Pill', 'url'=>array('create')),
	array('label'=>'Update Pill', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Pill', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Pill', 'url'=>array('admin')),
);
?>

<h1>View Pill #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'id',
		'name',
		'weight',
		'empty_weight',
		'dose',
		'default',
		'drug_id',
		'created',
		'updated',
	),
)); ?>