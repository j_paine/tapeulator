<?php
/* @var $this PillController */
/* @var $model Pill */
?>

<?php
$this->breadcrumbs=array(
	array('label'=>'Drugs','url'=>array('/drug')),
	array('label'=>$model->drug->name, 'url'=>array('/drug/view','id'=>$model->drug->id)),
	array('label'=>'Create'),
);

$this->menu=array(
	array('label'=>'List Pill', 'url'=>array('index')),
	array('label'=>'Manage Pill', 'url'=>array('admin')),
);
?>

<h1>Create Pill</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>