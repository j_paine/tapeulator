<?php
/* @var $this TaperController */
/* @var $model Taper */
?>

<?php
$this->breadcrumbs=array(
	array('label'=>'Drugs','url'=>array('/drug')),
	array('label'=>$model->drug->name, 'url'=>array('/drug/view','id'=>$model->drug->id)),
	array('label'=>'Taper'),
);

$this->menu=array(
	array('label'=>'List Taper', 'url'=>array('index')),
	array('label'=>'Create Taper', 'url'=>array('create')),
	array('label'=>'Update Taper', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Taper', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
if(Helpers::checkAdmin())
{
	$this->menu[]=array('label'=>'Manage Taper', 'url'=>array('admin'));
}
?>

<h1>View Taper #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'id',
		'drug_id',
		'start_dose',
		'end_dose',
		'step_size',
		'step_freq',
		'start_date',
		'end_date',
		'active',
		'created',
		'updated',
	),
)); ?>