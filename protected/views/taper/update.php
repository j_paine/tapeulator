<?php
/* @var $this TaperController */
/* @var $model Taper */
?>

<?php
$this->breadcrumbs=array(
	array('label'=>'Drugs','url'=>array('/drug')),
	array('label'=>$model->drug->name, 'url'=>array('/drug/view','id'=>$model->drug->id)),
	array('label'=>$model->id,'url'=>array('view','id'=>$model->id)),
	array('label'=>'Update'),
);

$this->menu=array(
	array('label'=>'List Taper', 'url'=>array('index')),
	array('label'=>'Create Taper', 'url'=>array('create')),
	array('label'=>'View Taper', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Taper', 'url'=>array('admin')),
);
?>

    <h1>Update Taper <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>