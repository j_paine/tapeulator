<?php
/* @var $this TaperController */
/* @var $model Taper */
?>

<?php
$this->breadcrumbs=array(
	array('label'=>'Drugs','url'=>array('/drug')),
	array('label'=>$model->drug->name, 'url'=>array('/drug/view','id'=>$model->drug->id)),
	array('label'=>'Add taper'),
);

$this->menu=array(
	array('label'=>'List Taper', 'url'=>array('index')),
	array('label'=>'Manage Taper', 'url'=>array('admin')),
);
?>

<h1>Create Taper</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>