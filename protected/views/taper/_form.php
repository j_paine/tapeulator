<?php
/* @var $this TaperController */
/* @var $model Taper */
/* @var $form TbActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'taper-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

            <?php echo $form->textFieldControlGroup($model,'start_dose',array('span'=>5)); ?>

            <?php echo $form->textFieldControlGroup($model,'end_dose',array('span'=>5)); ?>

            <?php echo $form->textFieldControlGroup($model,'step_size',array('span'=>5)); ?>

            <?php echo $form->textFieldControlGroup($model,'step_freq',array('span'=>5)); ?>

            <?php echo $form->textFieldControlGroup($model,'start_date',array('span'=>5)); ?>

            <?php echo $form->textFieldControlGroup($model,'end_date',array('span'=>5)); ?>

            <?php echo $form->textFieldControlGroup($model,'active',array('span'=>5)); ?>
			
			<?php echo $form->textFieldControlGroup($model, 'completed'); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		    'size'=>TbHtml::BUTTON_SIZE_LARGE,
		)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->