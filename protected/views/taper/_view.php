<?php
/* @var $this TaperController|DrugController */
/* @var $data Taper */
?>

<div class="view">

   	<?php if(get_class($this)==='TaperController'): ?>
		<b><?php echo CHtml::encode($data->getAttributeLabel('drug_id')); ?>:</b>
		<?php echo CHtml::encode($data->drug->name); ?>
		<br />
	<?php endif; ?>
		
	<b><?php echo CHtml::encode($data->getAttributeLabel('start_date')); ?>:</b>
	<?php echo CHtml::encode($data->start_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('start_dose')); ?>:</b>
	<?php echo CHtml::encode($data->start_dose); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('end_dose')); ?>:</b>
	<?php echo CHtml::encode($data->end_dose); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('step_size')); ?>:</b>
	<?php echo CHtml::encode($data->step_size); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('step_freq')); ?>:</b>
	<?php echo CHtml::encode($data->step_freq); ?>
	<br />

	

</div>