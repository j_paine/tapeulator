<?php
/* @var $this TaperController */
/* @var $model Taper */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

                    <?php echo $form->textFieldControlGroup($model,'id',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'drug_id',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'start_dose',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'end_dose',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'step_size',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'step_freq',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'start_date',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'end_date',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'active',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'created',array('span'=>5)); ?>

                    <?php echo $form->textFieldControlGroup($model,'updated',array('span'=>5)); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton('Search',  array('color' => TbHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->