<?php
/* @var $this TaperController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	array('label'=>'Tapers'),
);

$this->menu=array(
	array('label'=>'Create Taper','url'=>array('create')),
);
if(Helpers::checkAdmin())
{
	$this->menu[]=array('label'=>'Manage Taper', 'url'=>array('admin'));
}
?>

<h1>Tapers</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>