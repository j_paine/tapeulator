<?php
/* @var $this DrugController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	array('label'=>'Drugs'),
);

$this->menu=array(
	array('label'=>'Add drug','url'=>array('create')),
);
if(Yii::app()->user->name==='admin')
{
	$this->menu[]=array('label'=>'Manage drugs', 'url'=>array('admin'));
}
?>

<h1>Drugs</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>