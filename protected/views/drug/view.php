<?php
/* @var $this DrugController */
/* @var $model Drug */

/* @var $tapers CActiveDataProvider */
/* @var $pills CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	array('label'=>'Drugs','url'=>array('index')),
	array('label'=>$model->name),
);

$this->menu=array(
		array('label'=>'List drugs', 'url'=>array('index')),
		array('label'=>'Add drug', 'url'=>array('create')),
		array('label'=>'Edit drug', 'url'=>array('update', 'id'=>$model->id)),
		array('label'=>'Delete this drug', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>"Are you sure you want to delete this drug?\nAll associated information (tapers, pills, etc) will be removed")),
);
if(Helpers::checkAdmin())
{
	$this->menu[]=array('label'=>'Manage drugs', 'url'=>array('admin'));
}
?>

<h1><?php echo $model->name.' ('.$model->nick.')'; ?></h1>

<?php 
// Show minimal attributes unless admin
if(Helpers::checkAdmin())
{
	$attr=array(
		'id',
		'name',
		'nick',
		'half_life_low',
		'half_life_high',
		'user.username',
		'created',
		'updated',
	);
} else {
	$attr=array(
		
	);
}

$this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>$attr,
)); ?>

<h2>Doses</h2>

<?php $this->renderPartial('_doseGraph',array(
    'model'=>$model,
    'tapers'=>$tapers,
    'pills'=>$pills,
)); ?>

<?php echo TbHtml::button('Add doses', array(
	'submit'=>array('dose/create','id'=>$model->id),
	'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
	'style'=>'margin-bottom: 5px;',
)); ?>

<h2>Tapers</h2>

<?php echo TbHtml::button('Add taper', array(
	'submit'=>array('taper/create','id'=>$model->id),
	'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
	'style'=>'margin-bottom: 5px;',
)); ?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'drug-taper-grid',
	'dataProvider'=>$tapers,
	'columns'=>array(
		'drug.name',
		'start_dose',
		'end_dose',
		'step_size',
		'step_freq',
		'active',
		'completed',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{view} {update} {delete}',
			'buttons'=>array(
				'view'=>array(
					'label'=>'View taper',
					'url'=>'Yii::app()->createUrl("taper/view",array("id"=>$data->id))',
				),
				'update'=>array(
					'label'=>'Edit taper',
					'url'=>'Yii::app()->createUrl("taper/update",array("id"=>$data->id))',
				),
				'delete'=>array(
					'label'=>'Delete taper',
					'url'=>'Yii::app()->createUrl("taper/delete",array("id"=>$data->id))',
				),
			),
		),
	),
)); ?>



<h2>Pills</h2>

<?php echo TbHtml::button('Add pill', array(
	'submit'=>array('pill/create','id'=>$model->id),
	'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
	'style'=>'margin-bottom: 5px;',
)); ?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'drug-pill-grid',
	'type'=>'striped',
	'dataProvider'=>$pills,
	'columns'=>array(
		'name',
		'weight',
		'empty_weight',
		'dose',
		array(
			'class'=>'CCheckBoxColumn',
			'header'=>'Default',
			'id'=>'default',
			'selectableRows'=>1,
			'value'=>'$data->default',
			'checked'=>'$data->default==1',
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{view} {update} {delete}',
			'buttons'=>array(
				'view'=>array(
					'label'=>'View pill',
					'url'=>'Yii::app()->createUrl("pill/view",array("id"=>$data->id))',
				),
				'update'=>array(
					'label'=>'Edit pill',
					'url'=>'Yii::app()->createUrl("pill/update",array("id"=>$data->id))',
				),
				'delete'=>array(
					'label'=>'Delete pill',
					'url'=>'Yii::app()->createUrl("pill/delete",array("id"=>$data->id))',
				),
			),
		),
	),
)); ?>

<?php 
$url=Yii::app()->createUrl('pill/setDefault');
$cs = Yii::app()->clientScript->registerScript(
  'update-default-pill-on-check',
  "$('#drug-pill-grid :checkbox').change(function() {
	  $.ajax({
		type: 'POST',
		url: \"{$url}\",
		data: {
			id: $('#drug-pill-grid').yiiGridView('getChecked', 'default'),
			drug_id: \"{$model->id}\"
		}
	  });
	});"
);
?>