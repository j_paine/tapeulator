<?php
/* @var $this DrugController */
/* @var $model Drug */
/* @var $tapers CActiveDataProvider */
/* @var $pills CActiveDataProvider */
?>

<?php echo TbHtml::well('', array(
	'id'=>'drug-dose-graph',
	'style'=>'height: 200px;',
)); ?>
<?php $clientScript = Yii::app()->clientScript; ?>
<?php $clientScript->registerScriptFile('http://d3js.org/d3.v3.min.js', JClientScript::POS_END); ?>
<?php $clientScript->registerCssFile(Yii::app()->createAbsoluteUrl('../css/graph.css')); ?>
<?php $clientScript->beginScript('drug-dose-script', JClientScript::POS_END); ?>
<script type="text/javascript">
    var margin = {top: 20, right: 20, bottom: 30, left: 50},
        width = parseInt(d3.select("#drug-dose-graph").style("width")) - margin.left - margin.right,
        height = parseInt(d3.select("#drug-dose-graph").style("height")) - margin.top - margin.bottom;

    var x = d3.time.scale()
        .range([0, width]);

    var y = d3.scale.linear()
        .range([height, 0]);

    var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom");

    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left");
	
	resample;

    var line = d3.svg.line()
        .x(function(d) { return x(d.date); })
        .y(function(d) { return y(d.dose); });

    var svg = d3.select("#drug-dose-graph").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    d3.xhr("<?php echo Yii::app()->createUrl('/drug/getDoses', array('id'=>$model->id)); ?>")
        .header("Content-Type", "application/json")
        .post("", function(error, data) {
            data=JSON.parse(data.responseText);

            data.forEach(function(d) {
                    d.date=new Date(d.date.year,d.date.month,d.date.day);
            });

            x.domain(d3.extent(data, function(d) { return d.date; }));
            y.domain(d3.extent(data, function(d) { return d.dose; }));			

            svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis);

            svg.append("g")
                .attr("class", "y axis")
                .call(yAxis)
                .append("text")
                    .attr("transform", "rotate(-90)")
                    .attr("y", -40)
                    .attr("dy", ".71em")
                    .style("text-anchor", "end")
                    .text("Dose (mg)");

            svg.append("path")
                .datum(data)
                .attr("class", "line")
                .attr("d", line);
        });
	
		
	function resize() {
		var width = parseInt(d3.select("#drug-dose-graph").style("width")) - margin.left - margin.right,
			height = parseInt(d3.select("#drug-dose-graph").style("height")) - margin.top - margin.bottom;

		x.range([0,width]);
		y.range([height,0]);
		xAxis.ticks(Math.max(width/70, 2));
		yAxis.ticks(Math.max(height/70, 2));
		svg.select(".x.axis")
			.attr("transform", "translate(0," + height + ")")
			.call(xAxis);
		svg.select(".y.axis")
			.call(yAxis);
		svg.select(".line")
			.attr("d", line);
		resample;
		
	}
	
	function resample() {
		dataPerPixel = data.length/width;
		dataResampled = data.filter(
			function(d, i) { return i % Math.ceil(dataPerPixel) == 0; }
		);
	}
	
	d3.select(window).on('resize', resize);
</script>
<?php $clientScript->endScript(); ?>