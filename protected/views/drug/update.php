<?php
/* @var $this DrugController */
/* @var $model Drug */
?>

<?php
$this->breadcrumbs=array(
	array('label'=>'Drugs','url'=>array('index')),
	array('label'=>$model->name,'url'=>array('view','id'=>$model->id)),
	array('label'=>'Update'),
);

$this->menu=array(
	array('label'=>'List drugs', 'url'=>array('index')),
	array('label'=>'Add drug', 'url'=>array('create')),
	array('label'=>'Show this drug', 'url'=>array('view', 'id'=>$model->id)),
);
if(Yii::app()->user->name==='admin')
{
	$this->menu[]=array('label'=>'Manage drugs', 'url'=>array('admin'));
}
?>

<h1>Update Drug <?php echo $model->name.' ('.$model->nick.')'; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>