<?php
/* @var $this DrugController */
/* @var $data Drug */
?>

<div class="view">

	<b><?php echo CHtml::link(CHtml::encode($data->name),array('view','id'=>$data->id)); ?></b>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nick')); ?>:</b>
	<?php echo CHtml::encode($data->nick); ?>
	<br />

	<b>Half Life:</b>
	<?php echo CHtml::encode(Helpers::halfLifeRange($data->half_life_low,$data->half_life_high)); ?>
	<br />

	<b>Username:</b>
	<?php echo CHtml::encode($data->user->username); ?>
	<br />


</div>