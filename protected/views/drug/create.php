<?php
/* @var $this DrugController */
/* @var $model Drug */
?>

<?php
$this->breadcrumbs=array(
	array('label'=>'Drugs','url'=>array('index')),
	array('label'=>'Add drug'),
);

$this->menu=array(
	array('label'=>'List drugs', 'url'=>array('index')),
);
if(Yii::app()->user->name==='admin')
{
	$this->menu[]=array('label'=>'Manage drugs', 'url'=>array('admin'));
}
?>

<h1>Add New Drug</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>