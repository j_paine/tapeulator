<?php
/* @var $this DoseController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	array('label'=>'Doses'),
);

$this->menu=array(
	array('label'=>'Create Dose','url'=>array('create')),
	array('label'=>'Manage Dose','url'=>array('admin')),
);
?>

<h1>Doses</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>