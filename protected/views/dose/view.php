<?php
/* @var $this DoseController */
/* @var $model Dose */
?>

<?php
$this->breadcrumbs=array(
	array('label'=>'Doses','url'=>array('index')),
	array('label'=>$model->id),
);

$this->menu=array(
	array('label'=>'List Dose', 'url'=>array('index')),
	array('label'=>'Create Dose', 'url'=>array('create')),
	array('label'=>'Update Dose', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Dose', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Dose', 'url'=>array('admin')),
);
?>

<h1>View Dose #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'id',
		'pill_id',
		'taper_id',
		'weight',
		'date',
		'created',
		'updated',
	),
)); ?>