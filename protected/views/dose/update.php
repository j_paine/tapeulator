<?php
/* @var $this DoseController */
/* @var $model Dose */
?>

<?php
$this->breadcrumbs=array(
	array('label'=>'Doses','url'=>array('index')),
	array('label'=>$model->id,'url'=>array('view','id'=>$model->id)),
	array('label'=>'Update'),
);

$this->menu=array(
	array('label'=>'List Dose', 'url'=>array('index')),
	array('label'=>'Create Dose', 'url'=>array('create')),
	array('label'=>'View Dose', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Dose', 'url'=>array('admin')),
);
?>

    <h1>Update Dose <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>