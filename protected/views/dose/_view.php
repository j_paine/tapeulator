<?php
/* @var $this DoseController */
/* @var $data Dose */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('drug_id')); ?>:</b>
	<?php echo CHtml::encode($data->taper->drug->name); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('pill_id')); ?>:</b>
	<?php echo CHtml::encode($data->pill->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('taper_id')); ?>:</b>
	<?php echo CHtml::encode($data->taper_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('weight')); ?>:</b>
	<?php echo CHtml::encode($data->weight); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode(date_format(new DateTime($data->date), 'Y-m-d')); ?>
	<br />

</div>