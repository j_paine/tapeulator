<?php
/* @var $this DoseController */
/* @var $model Dose */

?>

<?php
$this->breadcrumbs=array(
	array('label'=>'Doses','url'=>array('index')),
	array('label'=>'Create'),
);

$this->menu=array(
	array('label'=>'List Dose', 'url'=>array('index')),
	array('label'=>'Manage Dose', 'url'=>array('admin')),
);
?>

<h1>Create Dose</h1>

<?php $this->renderPartial('_form', array(
	'model'=>$model,
)); ?>