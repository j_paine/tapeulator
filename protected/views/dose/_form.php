<?php
/* @var $this DoseController */
/* @var $model Dose */
/* @var $form TbActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('WhActiveForm', array(
	'id'=>'dose-form',
	'enableClientValidation'=>true,
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>
		
		<?php echo $form->dropDownListControlGroup($model, 'drug_id', TbHtml::listData(Drug::getDrugs(), 'id', 'name'),
			array(
				'ajax'=>array(
					'type'=>'POST',
					'url'=>Yii::app()->createUrl('/pill/getPills'),
					'update'=>'#'.TbHtml::activeId($model, 'pill_id'),
				),
			)); ?>
	
		<?php echo $form->dropDownListControlGroup($model, 'pill_id', array()); ?>
		
		<?php echo $form->textAreaControlGroup($model,'weight',array('wrap'=>'hard')); ?>
	
		<?php echo TbHtml::helpBlock('Tip: Enter multiple daily doses on separate lines to begin on the specified date'); ?>

		<?php $datePicker=$this->widget('yiiwheels.widgets.datepicker.WhDatePicker', array(
				'model'=>$model,
				'attribute'=>'date',
				'pluginOptions'=>array(
					'format'=>'yyyy-mm-dd',
				),
		), true); ?>
	
		<?php echo $form->customControlGroup($datePicker, $model, 'date'); ?>

        <div class="form-actions">
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		    'size'=>TbHtml::BUTTON_SIZE_LARGE,
		)); ?>
    </div>

    <?php $this->endWidget(); ?>
	
</div><!-- form -->

<?php $cs = Yii::app()->clientScript->registerScript(
  'update-pill-on-page-load',
  '$(document).ready(function() {
        $("#'.TbHtml::activeId($model, 'drug_id').'").change();
    });'
);
?>
