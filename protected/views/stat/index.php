<?php
/* @var $this StatController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	array('label'=>'Stats'),
);

$this->menu=array(
	array('label'=>'Create Stat','url'=>array('create')),
	array('label'=>'Manage Stat','url'=>array('admin')),
);
?>

<h1>Weight Log</h1>
<?php echo TbHtml::well('', array('class'=>'stat-graph')); ?>
<?php $clientScript = Yii::app()->clientScript; ?>
<?php $clientScript->registerScriptFile('http://d3js.org/d3.v3.min.js', JClientScript::POS_END); ?>
<?php $clientScript->registerCssFile(Yii::app()->createAbsoluteUrl('../css/graph.css')); ?>
<?php $clientScript->beginScript('my-script', JClientScript::POS_END); ?>
<script type="text/javascript">
    var margin = {top: 20, right: 20, bottom: 30, left: 30},
		width = 960 - margin.left - margin.right,
		height = 400 - margin.top - margin.bottom;

	var parseDate = d3.time.format("%Y-%m-%d").parse;

	var x = d3.time.scale()
		.range([0, width]);

	var y = d3.scale.linear()
		.range([height, 0]);
		
	var xAxis = d3.svg.axis()
		.scale(x)
		.orient("bottom");

	var yAxis = d3.svg.axis()
		.scale(y)
		.orient("left");
		

	var line = d3.svg.line()
		.x(function(d) { return x(d.date); })
		.y(function(d) { return y(d.weight); });

	//var circle = d3.svg.cir

	var svg = d3.select(".stat-graph").append("svg")
		.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.top + margin.bottom)
		.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
	
	d3.xhr("<?php echo Yii::app()->createUrl('/stat/getStats'); ?>")
		.header("Content-Type", "application/json")
		.post("", function(error, data) {
			data=JSON.parse(data.responseText);
			console.log(data);
			
			data.forEach(function(d) {
				d.date=new Date(d.date.year,d.date.month,d.date.day);
				console.log("Data id: " + d.id + " - Weight: " + d.weight + ", Date: " + d.date);
			});
			
			x.domain(d3.extent(data, function(d) { return d.date; }));
			y.domain(d3.extent(data, function(d) { return d.weight; }));
			

			svg.append("g")
				.attr("class", "x axis")
				.attr("transform", "translate(30," + height + ")")
				.call(xAxis);

			svg.append("g")
				.attr("class", "y axis")
				.call(yAxis)
				.append("text")
				.attr("transform", "rotate(-90)")
				.attr("y", 6)
				.attr("dy", ".71em")
				.style("text-anchor", "end")
				.text("Weight (kg)");
		
			svg.selectAll("circle")
				.data(data)
				.enter().append("circle")
				.attr("class", "circle")
				.attr("cx", function(d) { return x(d.date); })
				.attr("cy", function(d) { return y(d.weight); })
				.attr("r", 3);
			
			svg.append("path")
				.datum(data)
				.attr("class", "line")
				.attr("d", line);
		});
		
</script>
<?php $clientScript->endScript(); ?>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>