<?php
/* @var $this StatController */
/* @var $model Stat */
?>

<?php
$this->breadcrumbs=array(
	array('label'=>'Stats','url'=>array('index')),
	array('label'=>'Create'),
);

$this->menu=array(
	array('label'=>'List Stat', 'url'=>array('index')),
	array('label'=>'Manage Stat', 'url'=>array('admin')),
);
?>

<h1>Create Stat</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>