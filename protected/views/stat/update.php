<?php
/* @var $this StatController */
/* @var $model Stat */
?>

<?php
$this->breadcrumbs=array(
	array('label'=>'Stats','url'=>array('index')),
	array('label'=>$model->id,'url'=>array('view','id'=>$model->id)),
	array('label'=>'Update'),
);

$this->menu=array(
	array('label'=>'List Stat', 'url'=>array('index')),
	array('label'=>'Create Stat', 'url'=>array('create')),
	array('label'=>'View Stat', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Stat', 'url'=>array('admin')),
);
?>

    <h1>Update Stat <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>