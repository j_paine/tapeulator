<?php
/* @var $this StatController */
/* @var $model Stat */
?>

<?php
$this->breadcrumbs=array(
	array('label'=>'Stats','url'=>array('index')),
	array('label'=>$model->id),
);

$this->menu=array(
	array('label'=>'List Stat', 'url'=>array('index')),
	array('label'=>'Create Stat', 'url'=>array('create')),
	array('label'=>'Update Stat', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Stat', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Stat', 'url'=>array('admin')),
);
?>

<h1>View Stat #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView',array(
    'htmlOptions' => array(
        'class' => 'table table-striped table-condensed table-hover',
    ),
    'data'=>$model,
    'attributes'=>array(
		'id',
		'user_id',
		'weight',
		'date',
		'created',
		'updated',
	),
)); ?>