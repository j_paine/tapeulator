<?php
/* @var $this StatController */
/* @var $model Stat */
/* @var $form TbActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('WhActiveForm', array(
	'id'=>'stat-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

            <?php echo $form->textFieldControlGroup($model,'weight',array('span'=>5)); ?>

            <?php $datePicker = $this->widget('yiiwheels.widgets.datepicker.WhDatePicker', array(
				'model' => $model,
				'attribute' => 'date',
				'pluginOptions' => array(
					'format' => 'yyyy-mm-dd'
				),
			), true); ?>

		<?php echo $form->customControlGroup($datePicker, $model, 'date'); ?> 

        <div class="form-actions">
        <?php echo TbHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array(
		    'color'=>TbHtml::BUTTON_COLOR_PRIMARY,
		    'size'=>TbHtml::BUTTON_SIZE_LARGE,
		)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->

