<?php

/* Usage:
 * Add 'clientScript' component to main config
 * <?php $clientScript = Yii::app()->clientScript; ?>
 * <?php $clientScript->beginScript('my-script'); ?>
 * <script type="text/javascript">
 *   alert('my script');
 * </script>
 * <?php $clientScript->endScript(); ?>
 */

class JClientScript extends CClientScript {
	protected $activeScriptId;
	protected $activeScriptPosition;
	
	public function beginScript($id, $pos = parent::POS_READY)
	{
		$this->activeScriptId=$id;
		$this->activeScriptPosition=$pos;
		ob_start();
		ob_implicit_flush(false);
	}
	
	public function endScript() 
	{
		$script=ob_get_clean();
		$script=preg_replace('%</?script[^>]*>\s*%', '', $script);
		parent::registerScript($this->activeScriptId, $script, $this->activeScriptPosition);
	}
}