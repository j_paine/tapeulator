<?php

/* Extends CBreadcrumbs to add support for dynamic link labels
 * Use:
 *	$this->breadcrumbs=array(
 *		array('label'=>$label,'url'=>array($url,)),
 *	);
 * 
 */

Yii::import('zii.widgets.CBreadcrumbs');

class JBreadcrumbs extends CBreadcrumbs {
	
	/**
	 * Renders the content of the portlet.
	 */
	public function run()
	{
		if(empty($this->links))
			return;

		echo CHtml::openTag($this->tagName,$this->htmlOptions)."\n";
		$links=array();
		if($this->homeLink===null)
			$links[]=CHtml::link(Yii::t('zii','Home'),Yii::app()->homeUrl);
		elseif($this->homeLink!==false)
			$links[]=$this->homeLink;
		foreach($this->links as $link)
		{
			if(isset($link['url']) && is_array($link['url']))
				$links[]=strtr($this->activeLinkTemplate,array(
					'{url}'=>CHtml::normalizeUrl($link['url']),
					'{label}'=>$this->encodeLabel ? CHtml::encode($link['label']) : $link['label'],
				));
			else
				$links[]=str_replace('{label}',$this->encodeLabel ? CHtml::encode($link['label']) : $link['label'],$this->inactiveLinkTemplate);
		}
		echo implode($this->separator,$links);
		echo CHtml::closeTag($this->tagName);
	}
}