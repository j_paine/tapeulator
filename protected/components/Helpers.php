<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Helpers {
	
	/**
	 * Returns a half-life range as a formatted string
	 * @param float $low the low end of the half-life range
	 * @param float $high the high end of the half-life range
	 * @return string
	 */
	public static function halfLifeRange($low, $high)
	{
		if($low > 59)  $low=($low/60).' h';
		else		   $low=$low.' min';
		if($high > 59) $high=($high/60).' h';
		else		   $high=$high.' min';
		
		if($low > 0) {
			if($high > 0) return $low.' - '.$high;
			else		  return '>'.$low;
		} else {
			if($high > 0) return '<'.$high;
		}
		
	}
	
	/**
	 * Returns true if user is admin
	 * @return boolean
	 */
	public static function checkAdmin()
	{
		return Yii::app()->user->name==='admin';
	}
	
	/**
	 * Check if current user is authorized to act on/view a model
	 * @param CActiveRecord $model a model object with a user_id attribute
	 * @return boolean
	 */
	public static function checkAuth($model)
	{
		if($model) {
			if($model->hasAttribute('user_id')) return (self::checkAdmin() || $model->user_id===Yii::app()->user->id);
		} else {
			throw new CException('Invalid argument');
		}
		
	}
}