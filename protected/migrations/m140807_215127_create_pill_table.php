<?php

class m140807_215127_create_pill_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('pill', array(
				'id'=>'pk',
				'name'=>'string NOT NULL',
				'weight'=>'float NOT NULL',
				'empty_weight'=>'float',
				'dose'=>'float NOT NULL',
				'drug_id'=>'integer NOT NULL',
				'default'=>'boolean',
				'created'=>'datetime NOT NULL',
				'updated'=>'datetime DEFAULT NULL',
			));

		$this->addForeignKey('fk_pill_drug_drug', 'pill', 'drug_id', 'drug', 'id');
		$this->insert('pill', array(
			'name'=>'Mirtazepine Krka',
			'weight'=>460,
			'dose'=>45,
			'drug_id'=>1,
			'default'=>true,
			'created'=>new CDbExpression('NOW()'),
		));
	}

	public function down()
	{
		$this->dropTable('pill');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}