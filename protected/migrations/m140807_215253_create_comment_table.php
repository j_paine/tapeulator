<?php

class m140807_215253_create_comment_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('comment', array(
				'id'=>'pk',
				'content'=>'text NOT NULL',
				'dose_id'=>'integer',
				'created'=>'datetime NOT NULL',
				'updated'=>'datetime DEFAULT NULL',
			));

		$this->addForeignKey('fk_comment_dose_dose', 'comment', 'dose_id', 'dose', 'id');
		
	}

	public function down()
	{
		$this->dropTable('comment');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}