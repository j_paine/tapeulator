<?php

class m140807_215132_create_taper_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('taper', array(
				'id'=>'pk',
				'drug_id'=>'integer NOT NULL',
				'start_dose'=>'float NOT NULL',
				'end_dose'=>'float NOT NULL',
				'step_size'=>'float NOT NULL',
				'step_freq'=>'integer',
				'start_date'=>'datetime NOT NULL',
				'end_date'=>'datetime',
				'active'=>'boolean',
				'completed'=>'boolean',
				'created'=>'datetime NOT NULL',
				'updated'=>'datetime DEFAULT NULL',
			));

		$this->addForeignKey('fk_taper_drug_drug', 'taper', 'drug_id', 'drug', 'id');
		$this->insert('taper', array(
			'drug_id'=>1,
			'start_dose'=>45,
			'end_dose'=>0,
			'step_size'=>0.5,
			'step_freq'=>7,
			'active'=>true,
			'start_date'=>new CDbExpression('NOW()'),
			'created'=>new CDbExpression('NOW()'),
		));
	}

	public function down()
	{
		$this->dropTable('taper');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}