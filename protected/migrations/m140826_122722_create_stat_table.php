<?php

class m140826_122722_create_stat_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('stat', array(
				'id'=>'pk',
				'user_id'=>'integer NOT NULL',
				'weight'=>'float',
				'date'=>'datetime NOT NULL',
				'created'=>'datetime NOT NULL',
				'updated'=>'datetime DEFAULT NULL',
			));

		$this->addForeignKey('fk_stat_stat_user', 'stat', 'user_id', 'user', 'id');
		
	}

	public function down()
	{
		$this->dropTable('stat');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}