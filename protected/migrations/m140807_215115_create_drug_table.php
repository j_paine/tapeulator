<?php

class m140807_215115_create_drug_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('drug', array(
				'id'=>'pk',
				'name'=>'string NOT NULL',
				'nick'=>'string',
				'half_life_low'=>'float',
				'half_life_high'=>'float',
				'user_id'=>'integer NOT NULL',
				'created'=>'datetime NOT NULL',
				'updated'=>'datetime DEFAULT NULL',
			));

		$this->addForeignKey('fk_drug_user_user', 'drug', 'user_id', 'user', 'id');
		$this->insert('drug', array(
			'name'=>'Mirtazepine',
			'nick'=>'Remeron',
			'half_life_low'=>1200,
			'half_life_high'=>2400,
			'user_id'=>2,
			'created'=>new CDbExpression('NOW()'),
		));
		$this->insert('drug', array(
			'name'=>'Clonazepam',
			'nick'=>'Rivotril',
			'half_life_low'=>1080,
			'half_life_high'=>3000,
			'user_id'=>2,
			'created'=>new CDbExpression('NOW()'),
		));
		$this->insert('drug', array(
			'name'=>'Zuclopenthixol',
			'nick'=>'Cisordinol',
			'half_life_low'=>1200,
			'user_id'=>2,
			'created'=>new CDbExpression('NOW()'),
		));
	}

	public function down()
	{
		$this->dropTable('drug');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}