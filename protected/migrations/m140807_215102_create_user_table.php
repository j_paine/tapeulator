<?php

class m140807_215102_create_user_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('user', array(
			'id'=>'pk',
			'username'=>'VARCHAR(32) NOT NULL',
			'password'=>'string NOT NULL',
			'created'=>'datetime NOT NULL',
			'updated'=>'datetime DEFAULT NULL',
		));
		
		// Add unique index on username
		$this->createIndex('idx_user_username', 'user', 'username', true);
		$this->insert('user', array(
			'username'=>'admin',
			'password'=>  CPasswordHelper::hashPassword('pass'),
			'created'=>new CDbExpression('NOW()'),
		));
		$this->insert('user', array(
			'username'=>'eva',
			'password'=>CPasswordHelper::hashPassword('pass'),
			'created'=>new CDbExpression('NOW()'),
		));
	}

	public function down()
	{
		$this->dropTable('user');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}