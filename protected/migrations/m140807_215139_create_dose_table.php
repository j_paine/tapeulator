<?php

class m140807_215139_create_dose_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('dose', array(
				'id'=>'pk',
				'pill_id'=>'integer NOT NULL',
				'taper_id'=>'integer NOT NULL',
				'user_id'=>'integer NOT NULL',
				'weight'=>'float NOT NULL',
				'date'=>'datetime NOT NULL',
				'created'=>'datetime NOT NULL',
				'updated'=>'datetime DEFAULT NULL',
			));

		$this->addForeignKey('fk_dose_pill_pill', 'dose', 'pill_id', 'pill', 'id');
		$this->addForeignKey('fk_dose_taper_taper', 'dose', 'taper_id', 'taper', 'id');
		$this->addForeignKey('fk_dose_user_user', 'dose', 'user_id', 'user', 'id');
		
		$this->insert('dose', array(
			'pill_id'=>1,
			'taper_id'=>1,
			'user_id'=>1,
			'weight'=>380,
			'date'=>new CDbExpression('NOW()'),
			'created'=>new CDbExpression('NOW()'),
		));
	}

	public function down()
	{
		$this->dropTable('dose');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}